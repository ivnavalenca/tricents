package pages;

import infrastructure.domain.Insurance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static infrastructure.setup.Driver.getDriver;
import static infrastructure.setup.DriverUtil.*;

public class InsurancePage {

    public static final String URL = "https://sampleapp.tricentis.com/101/app.php";
    private final static By MAKE = By.id("make");
    private final static By MODEL = By.id("model");
    private final static By CYLINDER_CAPACITY = By.id("cylindercapacity");
    private final static By ENGINE_PERFORMANCE = By.id("engineperformance");
    private final static By DATE_OF_MANUFACTURE = By.id("dateofmanufacture");
    private final static By NUMBER_OF_SEATS = By.id("numberofseats");
    private final static By RIGHT_HAND_DRIVE = By.id("righthanddriveyes");
    private final static By NUMBER_OF_SEATS_MOTORCYCLE = By.id("numberofseatsmotorcycle");
    private final static By FUEL_TYPE = By.id("fuel");
    private final static By PAYLOAD = By.id("payload");
    private final static By TOTAL_WEIGHT = By.id("totalweight");
    private final static By LIST_PRICE = By.id("listprice");
    private final static By LICENSE_PLATE_NUMBER = By.id("licenseplatenumber");
    private final static By ANNUAL_MILEAGE = By.id("annualmileage");

    private final static By FIRST_NAME = By.id("firstname");
    private final static By LAST_NAME = By.id("lastname");
    private final static By BIRTHDATE = By.id("birthdate");

   // private final static By FEMALE_GENDER = By.cssSelector(".group:nth-child(2) > .ideal-radiocheck-label:nth-child(2) > .ideal-radio");
    private final static By STREET_ADDRESS = By.id("streetaddress");

    private final static By COUNTRY = By.id("country");

    private final static By ZIPCODE = By.id("zipcode");
    private final static By CITY = By.id("city");

    private final static By OCCUPATION = By.id("occupation");
    private final static By HOBBIES_SKYDIVING = By.cssSelector(".ideal-radiocheck-label:nth-child(4) > .ideal-check");
    private final static By WEBSITE = By.id("website");
    private final static By PICTURE = By.id("picture");
    private final static By START_DATE = By.id("startdate");
    private final static By INSURANCE_SUM = By.id("insurancesum");
    private final static By MERIT_RATING = By.id("meritrating");
    private final static By DAMAGE_INSURANCE = By.id("damageinsurance");
    private final static By EURO_PROTECTION_OPTIONAL_PRODUCT = By.cssSelector(".field:nth-child(5) .ideal-radiocheck-label:nth-child(1) > .ideal-check");
    private final static By COURTESY_CAR = By.id("courtesycar");
    private final static By GOLD_PRICE_OPTIONS = By.cssSelector(".choosePrice:nth-child(2) > .ideal-radio");
    private final static By EMAIL = By.id("email");
    private final static By PHONE = By.id("phone");
    private final static By USERNAME = By.id("username");

    private final static By PASSWORD = By.id("password");
    private final static By CONFIRM_PASSWORD = By.id("confirmpassword");
    private final static By COMMENTS = By.id("Comments");

    private final static By NEXT_ENTER_INSURANT_DATA_BTN = By.id("nextenterinsurantdata");
    private final static By NEXT_ENTER_PRODUCT_DATA_BTN = By.id("nextenterproductdata");
    private final static By NEXT_SELECT_PRICE_OPTION_BTN = By.id("nextselectpriceoption");

    private final static By NEXT_SEND_QUOTE_BTN = By.id("nextsendquote");
    private final static By SEND_EMAIL_BTN = By.id("sendemail");

    private final static By SUCCESS_MSG = By.cssSelector("h2");

    public void accessPage() {
        getDriver().get(URL);
    }

    public String getHomePageTitle() {
        return getDriver().getTitle();
    }

    public void createInsurance(Insurance insurance) {
        enterVehicleData(insurance);
        enterInsurantData(insurance);
        enterProductData(insurance);
        selectPriceOption(insurance);
        sendQuote(insurance);
    }

    private void enterVehicleData(Insurance insurance) {
        WebElement makeElement = waitForElementVisibility(MAKE);
        WebElement modelElement = waitForElementVisibility(MODEL);
        WebElement cylinderCapacityElement = waitForElementVisibility(CYLINDER_CAPACITY);
        WebElement enginePerformanceElement = waitForElementVisibility(ENGINE_PERFORMANCE);
        WebElement dateOfManufactureElement = waitForElementVisibility(DATE_OF_MANUFACTURE);
        WebElement numberOfSeatsElement = waitForElementVisibility(NUMBER_OF_SEATS);
        WebElement rightHandDriveElement = waitForElementVisibility(RIGHT_HAND_DRIVE);
        WebElement numberOfSeatsMotorcycleElement = waitForElementVisibility(NUMBER_OF_SEATS_MOTORCYCLE);
        WebElement fuelElement = waitForElementVisibility(FUEL_TYPE);
        WebElement payloadElement = waitForElementVisibility(PAYLOAD);
        WebElement totalWeightElement = waitForElementVisibility(TOTAL_WEIGHT);
        WebElement listPriceElement = waitForElementVisibility(LIST_PRICE);
        WebElement licensePlateElement = waitForElementVisibility(LICENSE_PLATE_NUMBER);
        WebElement annualMileageElement = waitForElementVisibility(ANNUAL_MILEAGE);

        WebElement nextEnterInsurantDataButton = waitForElementVisibility(NEXT_ENTER_INSURANT_DATA_BTN);

        selectDropdown(makeElement, insurance.getMake());
        selectDropdown(modelElement, insurance.getModel());
        inputTextField(cylinderCapacityElement, insurance.getCylinderCapacity());
        inputTextField(enginePerformanceElement, insurance.getEnginePerformance());
        inputTextField(dateOfManufactureElement, insurance.getDateOfManufacture());
        selectDropdown(numberOfSeatsElement, insurance.getNumberOfSeats());
        //RIGHT HAND DRIVE
        selectDropdown(numberOfSeatsMotorcycleElement, insurance.getNumberOfSeatsMotorcycle());
        selectDropdown(fuelElement, insurance.getFuelType());
        inputTextField(payloadElement, insurance.getPayload());
        inputTextField(totalWeightElement, insurance.getPayload());
        inputTextField(listPriceElement, insurance.getListPrice());
        inputTextField(licensePlateElement, insurance.getLicensePlateNumber());
        inputTextField(annualMileageElement, insurance.getAnnualMileage());
        nextEnterInsurantDataButton.click();
    }

    private void enterInsurantData(Insurance insurance) {
        WebElement firstNameElement = waitForElementVisibility(FIRST_NAME);
        WebElement lastNameElement = waitForElementVisibility(LAST_NAME);
        WebElement birthdateElement = waitForElementVisibility(BIRTHDATE);
       // WebElement femaleGenderElement = waitForElementVisibility(FEMALE_GENDER);
        WebElement streetAddressElement = waitForElementVisibility(STREET_ADDRESS);
        WebElement countryElement = waitForElementVisibility(COUNTRY);
        WebElement zipcodeElement = waitForElementVisibility(ZIPCODE);
        WebElement cityElement = waitForElementVisibility(CITY);
        WebElement occupationElement = waitForElementVisibility(OCCUPATION);
        //hobbies
        WebElement skydivingHobbieElement = waitForElementVisibility(HOBBIES_SKYDIVING);
        WebElement websiteElement = waitForElementVisibility(WEBSITE);
        WebElement pictureElement = waitForElementVisibility(PICTURE);
        WebElement nextEnterProductDataButton = waitForElementVisibility(NEXT_ENTER_PRODUCT_DATA_BTN);

        inputTextField(firstNameElement, insurance.getFirstName());
        inputTextField(lastNameElement, insurance.getLastName());
        inputTextField(birthdateElement, insurance.getDateOfBirth());
        //femaleGenderElement.click();
        inputTextField(streetAddressElement, insurance.getStreetAddress());
        selectDropdown(countryElement, insurance.getCountry());
        inputTextField(zipcodeElement, insurance.getZipCode());
        inputTextField(cityElement, insurance.getCity());
        selectDropdown(occupationElement, insurance.getOccupation());
        skydivingHobbieElement.click();
        inputTextField(websiteElement, insurance.getWebsite());
        nextEnterProductDataButton.click();
    }

    private void enterProductData(Insurance insurance) {
        WebElement startDateElement = waitForElementVisibility(START_DATE);
        WebElement insuranceSumElement = waitForElementVisibility(INSURANCE_SUM);
        WebElement meritRatingElement = waitForElementVisibility(MERIT_RATING);
        WebElement damageInsuranceElement = waitForElementVisibility(DAMAGE_INSURANCE);
        WebElement euroProtectionOptionalProduct = waitForElementVisibility(EURO_PROTECTION_OPTIONAL_PRODUCT);
        WebElement courtesyCarElement = waitForElementVisibility(COURTESY_CAR);

        WebElement nextSelectPriceOptionButton = waitForElementVisibility(NEXT_SELECT_PRICE_OPTION_BTN);

        inputTextField(startDateElement, insurance.getStartDate());
        selectDropdown(insuranceSumElement, insurance.getInsuranceSum());
        selectDropdown(meritRatingElement, insurance.getMeritRating());
        selectDropdown(damageInsuranceElement, insurance.getDamageInsurance());

        euroProtectionOptionalProduct.click();
        selectDropdown(courtesyCarElement, insurance.getCourtesyCar());
        nextSelectPriceOptionButton.click();
    }

    private void selectPriceOption(Insurance insurance) {
        WebElement goldPriceOptions = waitForElementVisibility(GOLD_PRICE_OPTIONS);
        goldPriceOptions.click();

        WebElement  nextSendQuoteButton = waitForElementVisibility(NEXT_SEND_QUOTE_BTN);
        nextSendQuoteButton.click();
    }

    private void sendQuote(Insurance insurance) {
        WebElement emailElement = waitForElementVisibility(EMAIL);
        WebElement phoneElement = waitForElementVisibility(PHONE);
        WebElement usernameElement = waitForElementVisibility(USERNAME);
        WebElement passwordElement = waitForElementVisibility(PASSWORD);
        WebElement confirmPasswordElement = waitForElementVisibility(CONFIRM_PASSWORD);
        WebElement commentsElement = waitForElementVisibility(COMMENTS);
        WebElement sendEmailButton = waitForElementVisibility(SEND_EMAIL_BTN);

        inputTextField(emailElement, insurance.getEmail());
        inputTextField(phoneElement, insurance.getPhone());
        inputTextField(usernameElement, insurance.getUsername());
        inputTextField(passwordElement, insurance.getPassword());
        inputTextField(confirmPasswordElement, insurance.getConfirmPassword());
        inputTextField(commentsElement, insurance.getComments());
        sendEmailButton.click();
    }

    public String getSuccessMessage() {
        WebElement successMsg = waitForElementVisibility(SUCCESS_MSG);
        return successMsg.getText();
    }

}