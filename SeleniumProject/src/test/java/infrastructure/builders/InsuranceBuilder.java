package infrastructure.builders;

import infrastructure.domain.Insurance;

public class InsuranceBuilder {

    private final String make = "Audi";
    private final String model = "Scooter";
    private final String cylinderCapacity = "500";
    private final String enginePerformance = "10";

    private final String dateOfManufacture = "01/02/2010";
    private final String numberOfSeats = "9";
    private final String rightHandDrive = "Yes";
    private final String numberOfSeatsMotorcycle = "2";
    private final String fuelType = "Gas";
    private final String payload = "100";
    private final String totalWeight = "100";
    private final String listPrice = "100000";
    private final String licensePlateNumber = "KQW1203";
    private final String annualMileage = "100000";
    private final String firstName = "Ivna";
    private final String lastName = "Oliveira";
    private final String dateOfBirth = "01/07/1985";
    private final String gender = "Female";
    private final String streetAddress = "Gaspar Perez";
    private final String country = "Brazil";
    private final String zipCode = "50670350";
    private final String city = "Recife";
    private final String occupation = "Employee";
    private final String hobbies = "Skydiving";
    private final String website = "http://www.google.com";
    private final String picture = "";
    private final String startDate = "02/12/2024";
    private final String insuranceSum = "7.000.000,00";
    private final String meritRating = "Bonus 4";
    private final String damageInsurance = "No Coverage";
    private final String optionalProducts = "Euro Protection";
    private final String courtesyCar = "Yes";
    private final String selectOption = "Gold";
    private final String email = "ivnavalenca@gmail.com";
    private final String phone = "81997292372";
    private final String username = "ivnavalenca";
    private final String password = "Password123456@";
    private final String confirmPassword = "Password123456@";
    private final String comments = "Adding some comments";

    public Insurance build() {
        return new Insurance(
                make,
                model,
                cylinderCapacity,
                enginePerformance,
                dateOfManufacture,
                numberOfSeats,
                rightHandDrive,
                numberOfSeatsMotorcycle,
                fuelType,
                payload,
                totalWeight,
                listPrice,
                licensePlateNumber,
                annualMileage,
                firstName,
                lastName,
                dateOfBirth,
                gender,
                streetAddress,
                country,
                zipCode,
                city,
                occupation,
                hobbies,
                website,
                picture,
                startDate,
                insuranceSum,
                meritRating,
                damageInsurance,
                optionalProducts,
                courtesyCar,
                selectOption,
                email,
                phone,
                username,
                password,
                confirmPassword,
                comments);
    }
}
