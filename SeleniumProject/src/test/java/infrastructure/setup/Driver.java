package infrastructure.setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Driver {

    private static WebDriver driver;

    public static long PAGE_LOAD_TIMEOUT = 60;

    public static WebDriver getDriver() {
        if(driver==null) {
            driver = createDriver();
            setupDriver();
        }
        return driver;
    }

    private static WebDriver createDriver() {
        String driverName = "webdriver.gecko.driver";
        String driverLocation = "libs/geckodriver.exe";
        System.setProperty(driverName, driverLocation);
        return new FirefoxDriver();
    }

    private static void setupDriver() {
        pageLoadTimeout();
        //windowFullscreen();
        deleteCookies();
    }

    public static void pageLoadTimeout() {
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, SECONDS);
    }

    public static void windowFullscreen() {
        driver.manage().window().fullscreen();
    }

    public static void deleteCookies() {
        driver.manage().deleteAllCookies();
    }

    public static void shutDown() {
        driver.quit();
        driver = null;
    }
}