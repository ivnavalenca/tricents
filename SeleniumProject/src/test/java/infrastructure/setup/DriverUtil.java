package infrastructure.setup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static infrastructure.setup.Driver.getDriver;

public class DriverUtil {

    public static long DEFAULT_WAIT_TIME = 20;

    public static WebElement waitForElementVisibility(By by) {
        return new WebDriverWait(getDriver(),DEFAULT_WAIT_TIME).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static List<WebElement> waitForElementsVisibility(By by) {
        return new WebDriverWait(getDriver(),DEFAULT_WAIT_TIME).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    public static void inputTextField(WebElement element, String text){
        element.clear();
        element.sendKeys(text);
    }
    public static void selectDropdown(WebElement element, String value){
        Select dropdown = new Select(element);
        dropdown.selectByVisibleText(value);
    }

}
