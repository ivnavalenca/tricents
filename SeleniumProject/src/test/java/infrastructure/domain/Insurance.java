package infrastructure.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Insurance {

    private String make;
    private String model;
    private String cylinderCapacity;
    private String enginePerformance;
    private String dateOfManufacture;
    private String numberOfSeats;
    private String rightHandDrive;
    private String numberOfSeatsMotorcycle;
    private String fuelType;
    private String payload;
    private String totalWeight;
    private String listPrice;
    private String licensePlateNumber;
    private String annualMileage;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String gender;
    private String streetAddress;
    private String country;
    private String zipCode;
    private String city;
    private String occupation;
    private String hobbies;
    private String website;
    private String picture;
    private String startDate;
    private String insuranceSum;
    private String meritRating;
    private String damageInsurance;
    private String optionalProducts;
    private String courtesyCar;
    private String selectOption;
    private String email;
    private String phone;
    private String username;
    private String password;
    private String confirmPassword;
    private String comments;
}

