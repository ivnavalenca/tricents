package tests;

import infrastructure.builders.InsuranceBuilder;
import infrastructure.domain.Insurance;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.InsurancePage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class InsuranceSteps {

    private String actualResult;
    private final InsurancePage insurancePage = new InsurancePage();
    private final InsuranceBuilder insuranceBuilder = new InsuranceBuilder();

    @Given("I access Insurance Page")
    public void iAccessInsurancePage() {
        insurancePage.accessPage();
    }

    @When("I insert Insurance Data")
    public void iInsertInsuranceData() {
        Insurance validInsurance = insuranceBuilder.build();
        insurancePage.createInsurance(validInsurance);
        actualResult = insurancePage.getSuccessMessage();
    }

    @Then("the result should be {string}")
    public void theResultShouldBe(String expectedResult) {
        assertThat(actualResult, is(expectedResult));
    }
}
