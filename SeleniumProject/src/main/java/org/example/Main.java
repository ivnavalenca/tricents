package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Main {
    public static void main(String[] args) {


        System.setProperty("webdriver.gecko.driver", "c:/projeto/SeleniumProject/SeleninumProject/libs/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();

        driver.get("https://sampleapp.tricentis.com/101/app.php");

        driver.findElement(By.id("make")).click();
        WebElement dropdown = driver.findElement(By.id("make"));
        dropdown.findElement(By.xpath("//option[. = 'Audi']")).click();
        driver.findElement(By.id("engineperformance")).click();
        driver.findElement(By.id("engineperformance")).sendKeys("10");
        driver.findElement(By.id("opendateofmanufacturecalender")).click();
        driver.findElement(By.id("dateofmanufacture")).sendKeys("01/02/2010");
        driver.findElement(By.id("numberofseats")).click();
        {
            WebElement dropdown1 = driver.findElement(By.id("numberofseats"));
            dropdown1.findElement(By.xpath("//option[. = '3']")).click();
        }
        driver.findElement(By.id("fuel")).click();
        {
            WebElement dropdown2 = driver.findElement(By.id("fuel"));
            dropdown2.findElement(By.xpath("//option[. = 'Petrol']")).click();
        }
        driver.findElement(By.id("listprice")).click();
        driver.findElement(By.id("listprice")).sendKeys("10000");
        driver.findElement(By.id("licenseplatenumber")).click();
        driver.findElement(By.id("licenseplatenumber")).sendKeys("KIQ2188");
        driver.findElement(By.id("annualmileage")).click();
        driver.findElement(By.id("annualmileage")).sendKeys("1000");
        driver.findElement(By.id("nextenterinsurantdata")).click();
        driver.findElement(By.id("firstname")).click();
        driver.findElement(By.id("firstname")).sendKeys("Ivna");
        driver.findElement(By.id("lastname")).sendKeys("Oliveira");
        driver.findElement(By.id("birthdate")).click();
        driver.findElement(By.id("birthdate")).sendKeys("01/07/1985");
        driver.findElement(By.cssSelector(".group:nth-child(2) > .ideal-radiocheck-label:nth-child(2) > .ideal-radio")).click();
        driver.findElement(By.id("streetaddress")).click();
        driver.findElement(By.id("streetaddress")).sendKeys("Gaspar Perez");
        driver.findElement(By.id("country")).click();
        {
            WebElement dropdown3 = driver.findElement(By.id("country"));
            dropdown3.findElement(By.xpath("//option[. = 'Brazil']")).click();
        }
        driver.findElement(By.id("zipcode")).click();
        driver.findElement(By.id("zipcode")).sendKeys("50670350");
        driver.findElement(By.id("city")).click();
        driver.findElement(By.id("city")).sendKeys("recife");
        driver.findElement(By.id("occupation")).click();
        {
            WebElement dropdown4 = driver.findElement(By.id("occupation"));
            dropdown4.findElement(By.xpath("//option[. = 'Employee']")).click();
        }
        driver.findElement(By.cssSelector(".ideal-radiocheck-label:nth-child(4) > .ideal-check")).click();
        driver.findElement(By.id("nextenterproductdata")).click();
        driver.findElement(By.id("openstartdatecalender")).click();
        driver.findElement(By.linkText("14")).click();
        driver.findElement(By.cssSelector("#openstartdatecalender > .fa")).click();
        driver.findElement(By.cssSelector(".ui-datepicker-next")).click();
        driver.findElement(By.linkText("18")).click();
        driver.findElement(By.id("insurancesum")).click();
        {
            WebElement dropdown5 = driver.findElement(By.id("insurancesum"));
            dropdown5.findElement(By.xpath("//option[. = '7.000.000,00']")).click();
        }
        driver.findElement(By.id("meritrating")).click();
        {
            WebElement dropdown6 = driver.findElement(By.id("meritrating"));
            dropdown6.findElement(By.xpath("//option[. = 'Bonus 4']")).click();
        }
        driver.findElement(By.id("damageinsurance")).click();
        {
            WebElement dropdown7 = driver.findElement(By.id("damageinsurance"));
            dropdown7.findElement(By.xpath("//option[. = 'No Coverage']")).click();
        }
        driver.findElement(By.cssSelector(".field:nth-child(5) .ideal-radiocheck-label:nth-child(1) > .ideal-check")).click();
        driver.findElement(By.id("courtesycar")).click();
        {
            WebElement dropdown8 = driver.findElement(By.id("courtesycar"));
            dropdown8.findElement(By.xpath("//option[. = 'No']")).click();
        }
        driver.findElement(By.id("nextselectpriceoption")).click();
        driver.findElement(By.cssSelector(".choosePrice:nth-child(2) > .ideal-radio")).click();
        driver.findElement(By.id("nextsendquote")).click();
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).sendKeys("ivnavalenca@gmail.com");
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("ivnavalenca");
        driver.findElement(By.id("password")).sendKeys("password12345CAPS");
        driver.findElement(By.id("confirmpassword")).sendKeys("password123456CAPS");
        driver.findElement(By.id("sendemail")).click();
        {
            WebElement element = driver.findElement(By.id("sendemail"));
            Actions builder = new Actions(driver);
            builder.moveToElement(element).perform();
        }
        {
            WebElement element = driver.findElement(By.tagName("body"));
            Actions builder = new Actions(driver);
            builder.moveToElement(element, 0, 0).perform();
        }
        driver.findElement(By.cssSelector(".confirm")).click();
        driver.findElement(By.cssSelector(".idealforms-field-password:nth-child(4)")).click();
    }
}